import argparse
parser = argparse.ArgumentParser(description='Branch Name')
parser.add_argument('--branch', dest='branch', type=str, help='Branch Name')
args = parser.parse_args()

st = args.branch
text_part = ""

for each in st:
    if each.isnumeric():
        break
    text_part += each
number_part = st.replace(text_part, "")

print(number_part)
