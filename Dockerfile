# Use an existing httpd image as the base
FROM httpd:latest

# Copy your local website files to the image
#COPY ./website/ /usr/local/apache2/htdocs/

# Expose port 80 to allow incoming HTTP requests
EXPOSE 80

